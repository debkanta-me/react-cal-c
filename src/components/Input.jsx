export function Input(props) {
  return <div style={styleInput}>{props.value}</div>;
}

const styleInput = {
  backgroundColor: "#3D0000",
  width: "7.6em",
  padding: "1em",
  height: "1em",
  color: "#fff",
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "flex-end",
  fontSize: "2.5rem",
  borderRadius: "5px",
  overflow: "hidden",
};

export default Input;
