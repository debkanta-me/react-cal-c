import React, { Component } from "react";

const normalBtn = {
  fontSize: "1em",
  fontWeight: "900",
  border: "1px solid gray",
  backgroundColor: "white",
  color: "gray",
  borderRadius: "5px",
};

export class button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }

  getStyle() {
    if (this.props.value === 0) {
      return {
        width: "24em",
        height: "5em",
        ...normalBtn,
      };
    } else if (isNaN(this.props.value) && this.props.value !== "clear") {
      return {
        width: "6em",
        height: "5em",
        ...normalBtn,
        backgroundColor: "#0F52BA",
        color: "white",
      };
    } else if (this.props.value === "clear") {
      return {
        width: "12em",
        height: "5em",
        ...normalBtn,
      };
    } else {
      return {
        width: "6em",
        height: "5em",
        ...normalBtn,
      };
    }
  }

  render() {
    return (
      <button
        style={this.getStyle()}
        onClick={this.props.handleButtonClick.bind(this, this.props.value)}
        className="btn"
      >
        {this.props.value}
      </button>
    );
  }
}

export default button;
