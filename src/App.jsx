import React, { Component } from "react";
import Button from "./components/Button";
import Input from "./components/Input";
import { evaluate } from "mathjs";
import "./App.css";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }

  handleButtonClick = (value) => {
    if (this.state.value === "Infinity" || this.state.value === "error") {
      this.setState({ value: "" });
    }

    if (value === "=") {
      let result;

      try {
        result = evaluate(this.state.value);

        if (!Number.isInteger(result)) {
          result = result.toFixed(3);
        }

        this.setState((prev) => {
          return {
            value: result.toString(),
          };
        });
      } catch (error) {
        this.setState({ value: "error" });
      }
    } else if (value === "clear") {
      this.setState({ value: "" });
    } else {
      this.setState((prev) => {
        if (
          isNaN(prev.value.slice(prev.value.length - 1, prev.value.length)) &&
          isNaN(value)
        ) {
          return {
            value: prev.value.slice(0, prev.value.length - 1) + value,
          };
        } else if (prev.value.length === 0 && isNaN(value)) {
          return {
            value: prev.value + "0" + value,
          };
        } else if (prev.value.length === 0 && !isNaN(value)) {
          return {
            value: prev.value + value,
          };
        } else if (prev.value.length > 0) {
          return {
            value: prev.value + value,
          };
        }
      });
    }
  };

  render() {
    return (
      <div className="app">
        <div className="container">
          <div className="row">
            <Input value={this.state.value} />
          </div>
          <div className="row">
            <Button
              value={"clear"}
              handleButtonClick={this.handleButtonClick}
            />
            <Button value={"="} handleButtonClick={this.handleButtonClick} />
            <Button value={"+"} handleButtonClick={this.handleButtonClick} />
          </div>
          <div className="row">
            <Button value={7} handleButtonClick={this.handleButtonClick} />
            <Button value={8} handleButtonClick={this.handleButtonClick} />
            <Button value={9} handleButtonClick={this.handleButtonClick} />
            <Button value={"-"} handleButtonClick={this.handleButtonClick} />
          </div>
          <div className="row">
            <Button value={4} handleButtonClick={this.handleButtonClick} />
            <Button value={5} handleButtonClick={this.handleButtonClick} />
            <Button value={6} handleButtonClick={this.handleButtonClick} />
            <Button value={"*"} handleButtonClick={this.handleButtonClick} />
          </div>
          <div className="row">
            <Button value={1} handleButtonClick={this.handleButtonClick} />
            <Button value={2} handleButtonClick={this.handleButtonClick} />
            <Button value={3} handleButtonClick={this.handleButtonClick} />
            <Button value={"/"} handleButtonClick={this.handleButtonClick} />
          </div>
          <div className="row">
            <Button value={0} handleButtonClick={this.handleButtonClick} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
